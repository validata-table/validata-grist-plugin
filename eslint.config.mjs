// @ts-check

import eslint from "@eslint/js";
import tseslint from "typescript-eslint";

export default tseslint.config({
  files: ["**/*.ts"],
  extends: [
    eslint.configs.recommended,
    ...tseslint.configs.recommended,
    ...tseslint.configs.stylistic,
  ],
  languageOptions: {
    parserOptions: { project: ["tsconfig.json"] },
  },
  rules: { "@typescript-eslint/no-floating-promises": "error" },
});
