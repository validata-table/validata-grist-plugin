# Objectif

L'objectif est d'offrir des fonctionnalités de validation directement dans 
Grist, sous la forme d'un plugin.  

Notamment, ce plugin permet une meilleure intégration des fonctionnalités dans 
le flux de travail des utilisateurs. 

Pour les utilisateurs de Grist, la validation peut se faire sans sortir de 
l'outil. Pour les non-utilisateurs, la validation se fait dans un 
environnement qui permet immédiatement les corrections, l'export des données 
corrigées, et sous réserve d'un second plugin concernant la publication sur 
data.gouv, la publication des données corrigées. 

# Récits utilisateurs

En tant qu'**utilisateur** :

... je souhaite valider les données sur lesquelles je travaille, sans sortir 
du document. 

... je souhaite pouvoir renseigner un schéma, soit en choisissant directement 
depuis le catalogue schema.data.gouv, soit en précisant une URL pointant vers 
le schéma, afin de renseigner le format attendu des données

... je souhaite pouvoir valider les données avec ce schéma.

... je souhaite pouvoir mettre à jour la validation si je change de schéma ou 
les données, afin de pouvoir corriger les données et avoir une boucle de 
rétroaction rapide. 

... je souhaite pouvoir visualiser clairement les erreurs générales (problème 
lors de la validation), afin d'être averti si quelque chose s'est mal passé. 

... je souhaite pouvoir visualiser clairement les erreurs d'en-tête (problème 
lors de la validation), afin d'identifier les colonnes manquantes ou 
surnuméraires.

... je souhaite pouvoir visualiser clairement et directement dans la table de 
données les cellules qui ont des erreurs, afin de pouvoir les identifier.

... je souhaite pouvoir naviguer d'une erreur à l'autre afin de pouvoir 
découvrir la position des erreurs y compris si la cellule ne s'affiche pas 
dans la vue actuelle de la table (par exemple : ne pas devoir scroller pour 
détecter une erreur à la 22.340 ème ligne)

... je peux obtenir facilement des détails sur l'erreur de validation des 
cellules qui ont des erreurs, afin de comprendre la nature de l'erreur.

... je souhaite pouvoir fermer et réouvrir mon document, en conservant l'état 
du plugin et de ses annotations associées.

... je souhaite pouvoir désactiver / fermer le plugin et faire disparaître 
toutes les annotations, afin de pouvoir retrouver la mise en forme initiale 
hors plugin. 

# Fonctionnalités MVP

- Les fonctionnalités se présentent sous la forme d'un plugin Grist

- À l'ouverture, le plugin offre un input pour préciser l'URL du schéma, et un 
  bouton "valider les données"

- À l'appui du bouton les données de la table sont validées via l'API Validata

- Les messages d'erreur générales et d'en-tête s'affichent dans la fenêtre du 
  plugin. 

- Concernant les erreurs de ligne ou de cellule :
    - Si aucune marque de formattage lié au plugin n'est présente, de 
      nouvelles marques de formattage sont ajoutées pour colorer en rouge les 
      cellules qui contiennent des erreurs. Les erreurs de lignes colorent 
      toute la ligne. 
    - Si certaines marques sont déjà présentes, alors celles-ci sont 
      mises-à-jour. Les marques manquantes (par exemple, ajout de colonne, ou 
      suppression manuelle de l'utilisateur) sont créées. 

- La sélection d'une ligne avec des erreurs affichent les noms de colonnes 
  avec erreur, suivis du ou des messages d'erreur associés. 

- La fermeture du plugin retire toutes les marques d'annotation.

# Fonctionnalités backlog

- Menu déroulant pour sélectionner un schéma depuis schema.data.gouv
- Des boutons "Previous", "Next", qui sélectionnent  et affichent la 
  précédente ou prochaine ligne avec des erreurs, le cas échéant.
