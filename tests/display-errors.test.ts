import {
  DummyGristService,
  GristTestService,
  ValidataTestService,
  DOMTestService,
  makeTestError,
  makeTableData,
} from "./testing";

import {
  validateTable,
  displayRowErrors,
  getValidationReport,
} from "../src/plugin";

import { type ValidationReport, Tag } from "../src/types/report";

const TEST_COLUMN = "test";

test(`GIVEN a Validata Report with an error
       WHEN the error is displayed to the user
       THEN structure, body and line errors are properly separated
      `, async function () {
  //

  const gristService = new DummyGristService();

  enum ErrorType {
    Structure,
    Body,
    Row,
  }

  const testCases = [
    {
      name: "missing tags -> structure",
      tags: [],
      expected: ErrorType.Structure,
    },
    {
      tags: [Tag.Structure],
      expected: ErrorType.Structure,
    },
    {
      tags: [Tag.Head],
      expected: ErrorType.Structure,
    },
    {
      tags: [Tag.Header],
      expected: ErrorType.Structure,
    },
    {
      tags: [Tag.Body],
      expected: ErrorType.Body,
    },
    {
      tags: [Tag.Content],
      expected: ErrorType.Body,
    },
    {
      tags: [Tag.Table],
      expected: ErrorType.Body,
    },
    {
      tags: [Tag.Cell],
      expected: ErrorType.Row,
    },
    {
      tags: [Tag.Row],
      expected: ErrorType.Row,
    },
    {
      name: "row tag has higher priority 1",
      tags: [Tag.Table, Tag.Cell],
      expected: ErrorType.Row,
    },
    {
      name: "row tag has higher priority 2",
      tags: [Tag.Cell, Tag.Table],
      expected: ErrorType.Row,
    },
    {
      name: "row tag has higher priority 3",
      tags: [Tag.Table, Tag.Row],
      expected: ErrorType.Row,
    },
  ];

  for (const tc of testCases) {
    const validataService = new ValidataTestService([
      makeTestError(TEST_COLUMN, 1, { tags: tc.tags }),
    ]);

    const domService = new DOMTestService();

    await validateTable("", validataService, gristService, domService);

    const report = getValidationReport();
    if (report) {
      displayRowErrors(report, 1, domService);
    }

    expect(domService._bodyErrors).toHaveLength(
      tc.expected == ErrorType.Body ? 1 : 0,
    );
    expect(domService._rowErrors).toHaveLength(
      tc.expected == ErrorType.Row ? 1 : 0,
    );
    expect(domService._structureErrors).toHaveLength(
      tc.expected == ErrorType.Structure ? 1 : 0,
    );
  }
});

test(`GIVEN records, with possibly unsorted ids
       WHEN selecting a given row of data
       THEN the plugin prints errors related to this row of data`, async function () {
  //
  const table = makeTableData(
    [{ id: TEST_COLUMN, values: ["a", "b"] }],
    [2, 1],
  );

  const gristService = new GristTestService(table, {});

  const domService = new DOMTestService();

  const testCases = [
    {
      testErrors: [makeTestError(TEST_COLUMN, 1)], // $id == 2
      selectedRowIndex: 0, // zero indexed
      expectedLength: 1,
    },
    {
      testErrors: [makeTestError(TEST_COLUMN, 1)], // $id == 2
      selectedRowIndex: 1,
      expectedLength: 0,
    },
    {
      testErrors: [
        makeTestError(TEST_COLUMN, 1), // $id == 2
        makeTestError(TEST_COLUMN, 1), // $id == 2
        makeTestError(TEST_COLUMN, 2), // $id == 1
      ],
      selectedRowIndex: 0,
      expectedLength: 2,
    },
    {
      testErrors: [
        makeTestError(TEST_COLUMN, 1), // $id == 2
        makeTestError(TEST_COLUMN, 1), // $id == 2
        makeTestError(TEST_COLUMN, 2), // $id == 1
      ],
      selectedRowIndex: 1,
      expectedLength: 1,
    },
  ];

  for (const tc of testCases) {
    const validataService = new ValidataTestService(tc.testErrors);
    await validateTable("", validataService, gristService, domService);

    const report: ValidationReport | undefined = getValidationReport();

    const rowId = table.idColumn.values[tc.selectedRowIndex];

    if (report) {
      displayRowErrors(report, rowId, domService);
    }

    expect(domService._rowErrors).toHaveLength(tc.expectedLength);
  }
});
