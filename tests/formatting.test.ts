import { validateTable } from "../src/plugin";

import {
  GristTestService,
  ValidataTestService,
  DOMTestService,
  makeTestError,
  makeTableData,
} from "./testing";

const TEST_COLUMN = "TestColumn";
const TEST_DATA = makeTableData([{ id: TEST_COLUMN, values: ["A"] }]);

test(`GIVEN already existing conditional formatting associated to the plugin on a column
       WHEN the data is validated
       THEN the conditional formatting is expected to be updated instead of
            re-created
      `, async function () {
  //
  const initialConditionalFormattings = {
    [TEST_COLUMN]: [
      {
        formula: "True",
        isWatermarked: true,
      },
    ],
  };

  test;

  const gristService = new GristTestService(
    TEST_DATA,
    initialConditionalFormattings,
  );

  const validataService = new ValidataTestService([
    makeTestError(TEST_COLUMN, 1, { fieldNumber: 1 }),
  ]);

  const domService = new DOMTestService();

  await validateTable("", validataService, gristService, domService);

  expect(gristService._conditionalFormattings[TEST_COLUMN]).toHaveLength(1);
  expect(
    gristService._conditionalFormattings[TEST_COLUMN][0].isWatermarked,
  ).toBe(true);
  expect(gristService._conditionalFormattings[TEST_COLUMN][0].formula).toBe(
    "$id in [1]",
  );

  // Case where there is no longer an error, and formatting needs to be
  // removed

  const gristService2 = new GristTestService(
    TEST_DATA,
    initialConditionalFormattings,
  );

  const validataService2 = new ValidataTestService([]);

  const domService2 = new DOMTestService();

  await validateTable("", validataService2, gristService2, domService2);

  expect(gristService._conditionalFormattings[TEST_COLUMN] || []).toHaveLength(
    0,
  );
});

test(`GIVEN no existing conditional formatting associated to the plugin on a column
       WHEN the data is validated
       THEN the conditional formatting is expected to be created
      `, async function () {
  //
  const gristService = new GristTestService(TEST_DATA, {});

  const validataService = new ValidataTestService([
    makeTestError(TEST_COLUMN, 1, { fieldNumber: 1 }),
  ]);

  const domService = new DOMTestService();

  await validateTable("", validataService, gristService, domService);

  expect(gristService._conditionalFormattings[TEST_COLUMN]).toHaveLength(1);
  expect(
    gristService._conditionalFormattings[TEST_COLUMN][0].isWatermarked,
  ).toBe(true);
  expect(gristService._conditionalFormattings[TEST_COLUMN][0].formula).toBe(
    "$id in [1]",
  );
});

test(`GIVEN data with lines not sorted by (grist internal) $id
       WHEN the data is validated
       THEN the conditional formatting is applied on the correct cells`, async function () {
  const gristService = new GristTestService(
    makeTableData([{ id: TEST_COLUMN, values: ["a", "b"] }], [2, 1]),
    {},
  );
  const validataService = new ValidataTestService([
    makeTestError(TEST_COLUMN, 1),
  ]);
  const domService = new DOMTestService();

  await validateTable("", validataService, gristService, domService);

  expect(gristService._conditionalFormattings[TEST_COLUMN][0].formula).toBe(
    "$id in [2]",
  );
});

test(`GIVEN table data where labels differ from column ids
       WHEN conditional formatting is updated
       THEN columns are properly identified by their column string ids`, async function () {
  const TEST_ID = "id2";
  const TEST_LABEL = "ID";

  const data = makeTableData([
    { id: TEST_ID, label: TEST_LABEL, values: ["a", "b"] },
  ]);
  const gristService = new GristTestService(data, {});

  const validataService = new ValidataTestService([
    makeTestError(TEST_LABEL, 1),
  ]);

  const domService = new DOMTestService();

  await validateTable("", validataService, gristService, domService);

  expect(Object.keys(gristService._conditionalFormattings)[0]).toBe(TEST_ID);
});

test.skip(`GIVEN table data where labels differ from column ids
            WHEN conditional formatting is updated
            THEN columns are properly identified by their column string ids`, async function () {
  const TEST_ID_1 = "id1";
  const TEST_ID_2 = "id2";
  const DUPLICATED_LABEL = "ID";

  const data = makeTableData([
    { id: TEST_ID_1, label: DUPLICATED_LABEL, values: ["a", "b"] },
    { id: TEST_ID_2, label: DUPLICATED_LABEL, values: ["a", "b"] },
  ]);
  const gristService = new GristTestService(data, {});

  const validataService = new ValidataTestService([
    makeTestError(DUPLICATED_LABEL, 1),
  ]);

  const domService = new DOMTestService();

  await validateTable("", validataService, gristService, domService);

  expect(Object.keys(gristService._conditionalFormattings)[0]).toBe(TEST_ID_2);
});
