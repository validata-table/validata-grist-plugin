import type { IGrist, IValidata, IDOM } from "../src/spi";
import { type Error, Tag } from "../src/types/report";
import type { TableData, Column, IdColumn } from "../src/types/records";

type ColStrId = string;

/* Watermarked means that it has been created by this plugin */
type ConditionalFormatting = { formula: string; isWatermarked: boolean };

type ConditionalFormattings = Record<ColStrId, ConditionalFormatting[]>;

export class GristTestService implements IGrist {
  _data: TableData;
  _conditionalFormattings: ConditionalFormattings;

  constructor(data: TableData, conditionalFormattings: ConditionalFormattings) {
    this._data = data;
    this._conditionalFormattings = conditionalFormattings;
  }

  async fetchRecords(): Promise<TableData | undefined> {
    return Promise.resolve(this._data);
  }

  async hasConditionalFormatting(columnStrId: string): Promise<boolean> {
    if (columnStrId in this._conditionalFormattings) {
      for (const cf of this._conditionalFormattings[columnStrId]) {
        if (cf.isWatermarked) {
          return true;
        }
      }
    }

    return false;
  }

  async newConditionalFormatting(
    columnStrId: string,
    formula: string,
  ): Promise<void> {
    if (!(columnStrId in this._conditionalFormattings)) {
      this._conditionalFormattings[columnStrId] = [];
    }
    this._conditionalFormattings[columnStrId].push({
      formula: formula,
      isWatermarked: true,
    });

    return Promise.resolve();
  }

  async updateConditionalFormatting(
    columnStrId: string,
    formula: string,
  ): Promise<void> {
    if (columnStrId in this._conditionalFormattings) {
      for (const cf of this._conditionalFormattings[columnStrId]) {
        if (cf.isWatermarked) {
          cf.formula = formula;
          return Promise.resolve();
        }
      }
    }
    throw new Error(
      "Trying to update a conditional formatting which does not exist",
    );
  }

  async deleteConditionalFormatting(columnStrId: string): Promise<void> {
    if (columnStrId in this._conditionalFormattings) {
      for (const [index, cf] of this._conditionalFormattings[
        columnStrId
      ].entries()) {
        if (cf.isWatermarked) {
          this._conditionalFormattings[columnStrId].splice(index, 1);
        }
      }
    }
  }
}

export class DummyGristService extends GristTestService {
  /** Grist service that returns data with 5 rows and no columns.

  Row ids are necessary for mapping errors to rows**/
  constructor() {
    const dummyData = makeTableData([], [1, 2, 3, 4, 5]);
    const noConditionalFormatting: ConditionalFormattings = {};
    super(dummyData, noConditionalFormatting);
  }
}

export interface TestError extends Error {}

class OptionalTestFields {
  fieldNumber: number = 1;
  tags: Tag[] = [Tag.Cell];
}

export function makeTestError(
  fieldName: string,
  rowNumber: number,
  optional: Partial<OptionalTestFields> = {},
): TestError {
  const optionalWithDefaults: OptionalTestFields = {
    ...new OptionalTestFields(),
    ...optional,
  };

  return {
    code: "test-error",
    description: "",
    fieldNumber: optionalWithDefaults.fieldNumber,
    fieldName: fieldName,
    labels: [],
    message: "",
    name: "",
    note: "",
    rowNumber: rowNumber,
    rowId: rowNumber,
    tags: optionalWithDefaults.tags,
  };
}

export class ValidataTestService implements IValidata {
  _errors: TestError[];

  constructor(testErrors: TestError[]) {
    this._errors = testErrors;
  }

  async requestValidataReport(
    _dataRecords: TableData,
    _schemaURL: string,
    _options = { header_case: true },
  ): Promise<Object> {
    return { report: { tasks: [{ errors: this._errors }] } };
  }
}

export class DOMTestService implements IDOM {
  _structureErrors: Error[] = [];
  _bodyErrors: Error[] = [];
  _rowErrors: Error[] = [];

  printStructureErrors(errs: Error[]) {
    this._structureErrors = errs;
  }
  printBodyErrors(errs: Error[]) {
    this._bodyErrors = errs;
  }
  printRowErrors(errs: Error[]) {
    this._rowErrors = errs;
  }
}

export function makeTableData(
  columns: {
    id: string;
    values: any[];
    label?: string | undefined;
  }[],
  ids: number[] | undefined = undefined,
): TableData {
  let nRows = 0;
  if (columns.length > 0) {
    nRows = columns[0].values.length;
  }

  if (!ids) {
    ids = Array.from({ length: nRows }, (_, key) => key + 1);
  }

  const idColumn: IdColumn = {
    id: "id",
    label: "",
    position: -1,
    values: ids,
  };

  const dataColumns: Column[] = [];
  columns.forEach((column, idx) => {
    dataColumns.push({
      id: column.id,
      position: idx,
      values: column.values,
      label: column.label ? column.label : column.id,
    });
  });

  return {
    nRows: nRows,
    idColumn: idColumn,
    columns: dataColumns,
    tableId: "table1",
  };
}
