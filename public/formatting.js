var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
export function highlightErrors(report, table, gristService) {
    return __awaiter(this, void 0, void 0, function* () {
        const errorRowsByLabel = aggregateErrorRowsByLabel(report);
        const promises = [];
        for (const label of table.columns.map((col) => col.label)) {
            const columnHasErrors = label in errorRowsByLabel;
            let errorRowIds;
            if (columnHasErrors) {
                const errorRows = errorRowsByLabel[label];
                errorRowIds = errorRows.map((n) => {
                    return table.idColumn.values[n - 1];
                });
            }
            else {
                errorRowIds = [];
            }
            const colStrId = findStrIdFromLabel(label, table);
            if (!colStrId) {
                return;
            }
            promises.push(updateOrCreateConditionalFormatting(colStrId, errorRowIds, gristService));
        }
        yield Promise.all(promises);
    });
}
function findStrIdFromLabel(label, table) {
    const column = table.columns.find((col) => col.label == label);
    if (!column) {
        return;
    }
    return column.id;
}
/**
 * Returns for each column an array of row numbers containing invalid values
 */
function aggregateErrorRowsByLabel(report) {
    var _a, _b;
    const errorRowsByLabel = {};
    const reportedCellErrors = ((_b = (_a = report === null || report === void 0 ? void 0 : report.report) === null || _a === void 0 ? void 0 : _a.tasks[0]) === null || _b === void 0 ? void 0 : _b.errors) || [];
    for (const err of reportedCellErrors) {
        if (err.fieldName && err.rowNumber) {
            if (!errorRowsByLabel[err.fieldName]) {
                errorRowsByLabel[err.fieldName] = [];
            }
            errorRowsByLabel[err.fieldName].push(err.rowNumber);
        }
    }
    return errorRowsByLabel;
}
function updateOrCreateConditionalFormatting(colStrId, errorRowIds, gristService) {
    return __awaiter(this, void 0, void 0, function* () {
        const formula = `$id in [${errorRowIds}]`;
        const hasFormatting = yield gristService.hasConditionalFormatting(colStrId);
        const hasErrors = errorRowIds.length > 0;
        if (hasFormatting && !hasErrors) {
            yield gristService.deleteConditionalFormatting(colStrId);
        }
        if (hasFormatting && hasErrors) {
            yield gristService.updateConditionalFormatting(colStrId, formula);
        }
        if (!hasFormatting && hasErrors) {
            yield gristService.newConditionalFormatting(colStrId, formula);
        }
    });
}
