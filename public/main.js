var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { validateTable, getValidationReport } from "./plugin.js";
import { displayRowErrors } from "./plugin.js";
import { GristService } from "./infra/grist.js";
import { DOMService } from "./infra/dom.js";
import { ValidataService } from "./infra/validata.js";
const gristService = new GristService();
const domService = new DOMService();
const validataService = new ValidataService();
grist.ready({
    requiredAccess: "full",
});
grist.onRecord((row) => __awaiter(void 0, void 0, void 0, function* () {
    const report = getValidationReport();
    if (report && row) {
        displayRowErrors(report, row.id, domService);
    }
}));
const SCHEMA_FORM_ID = "schema-form";
const schemaForm = document.getElementById(SCHEMA_FORM_ID);
// Any form submit (re)triggers a validation
if (schemaForm) {
    schemaForm.addEventListener("submit", validate);
}
function validate(event) {
    return __awaiter(this, void 0, void 0, function* () {
        event.preventDefault(); // do not refresh page
        const schemaURL = _get_schema_url(event);
        if (schemaURL) {
            void validateTable(schemaURL, validataService, gristService, domService);
        }
    });
}
/**
 * @returns <?string> URL to a Table schema descriptor
 */
function _get_schema_url(event) {
    const target = event.target;
    if (!target || !(target instanceof HTMLFormElement)) {
        return;
    }
    const data = new FormData(target);
    const schema = data.get("schema");
    if (!schema || !(typeof schema === "string")) {
        console.error("The schema input is not defined, not provided as a string, or empty (i.e. not set as required). This is an internal error.");
        return;
    }
    return schema;
}
