export var Tag;
(function (Tag) {
    Tag["Head"] = "#head";
    Tag["Structure"] = "#structure";
    Tag["Header"] = "#header";
    Tag["Body"] = "#body";
    Tag["Cell"] = "#cell";
    Tag["Content"] = "#content";
    Tag["Row"] = "#row";
    Tag["Table"] = "#table";
})(Tag || (Tag = {}));
function hasTagFromSet(err, set) {
    return err.tags.some((tag) => set.has(tag));
}
export function relatesToDataStructure(err) {
    return err.tags.length == 0 || hasTagFromSet(err, STRUCTURE_TAGS);
}
export function relatesToDataBody(err) {
    return hasTagFromSet(err, BODY_TAGS);
}
export function relatesToRow(err) {
    return hasTagFromSet(err, ROW_TAGS);
}
const STRUCTURE_TAGS = new Set([
    Tag.Head,
    Tag.Structure,
    Tag.Header,
]);
const BODY_TAGS = new Set([
    Tag.Body,
    Tag.Content,
    Tag.Table,
]);
const ROW_TAGS = new Set([Tag.Cell, Tag.Row]);
