var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { highlightErrors } from "./formatting.js";
import { relatesToDataStructure, relatesToDataBody, relatesToRow, } from "./types/report.js";
let validationReport = undefined;
/** Validate data against the user provided schema.
 *
 * Cell and Line error messages are only displayed after the user selects the
 * a given line with errors.
 */
export function validateTable(schemaURL, validataService, gristService, domService) {
    return __awaiter(this, void 0, void 0, function* () {
        const records = yield gristService.fetchRecords();
        if (records) {
            const report = yield validataService.requestValidataReport(records, schemaURL, {
                header_case: true,
            });
            addRowIds(report, records);
            storeValidationReport(report);
            void highlightErrors(report, records, gristService);
            displayGeneralErrors(report, domService);
        }
    });
}
/** Returns the last evaluated validation report, if any.
 */
export function getValidationReport() {
    return validationReport;
}
/** Stores a validation report for future reference with `getValidationReport`
 */
function storeValidationReport(report) {
    validationReport = report;
}
/** Given a report, displays all table errors to the user
 */
export function displayGeneralErrors(report, domService) {
    domService.printStructureErrors(extractStructureErrors(report));
    domService.printBodyErrors(extractBodyErrors(report));
}
/** Given a report, displays errors specific to line n to the user
 */
export function displayRowErrors(report, rowId, domService) {
    domService.printRowErrors(extractRowErrors(report, rowId));
}
function extractStructureErrors(report) {
    const errors1 = report.report.errors || [];
    let errors2 = [];
    if (report.report.tasks.length > 0) {
        errors2 = report.report.tasks[0].errors;
    }
    const allErrors = errors1.concat(errors2);
    return allErrors.filter(relatesToDataStructure);
}
function extractBodyErrors(report) {
    const errors1 = report.report.errors || [];
    let errors2 = [];
    if (report.report.tasks.length > 0) {
        errors2 = report.report.tasks[0].errors;
    }
    const allErrors = errors1.concat(errors2);
    return allErrors
        .filter(relatesToDataBody)
        .filter((err) => !relatesToRow(err));
}
function extractRowErrors(report, rowId) {
    if (report.report.tasks.length == 0) {
        return [];
    }
    return report.report.tasks[0].errors
        .filter(relatesToRow)
        .filter((err) => err.rowId == rowId);
}
function addRowIds(report, table) {
    for (const error of report.report.tasks[0].errors) {
        if (error.rowNumber) {
            const rowId = table.idColumn.values[error.rowNumber - 1];
            error.rowId = rowId;
        }
    }
}
