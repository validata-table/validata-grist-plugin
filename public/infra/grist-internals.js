var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
/**
 * An helper class for representing and manipulating column data
 *
 * It make it more easy to manipulate information about columns, especially by
 * enabling the access of information about a specific column with its string
 * id.
 *
 * In other words, it transforms the raw columnar representation of internal
 * column information (type _InternalColumnsRepr) to access a row by column string
 * id (`colId` property)
 */
export class ColumnsInfo {
    static init() {
        return __awaiter(this, void 0, void 0, function* () {
            return new ColumnsInfo(yield this.fetchData());
        });
    }
    /** Data of a `ColumnsInfo` does not update automatically, an explicit call
     * to `update` is necessary to fetch new data.
     *
     * It is in fact the same as initiate the object again.
     */
    update() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield ColumnsInfo.init();
        });
    }
    constructor(internalColumnsRepr) {
        this.internalColumnsRepr = internalColumnsRepr;
        this.indexById = internalColumnsRepr.id.reduce((accu, _, idx) => {
            const id = internalColumnsRepr.id[idx];
            accu[id] = idx;
            return accu;
        }, {});
    }
    getIdFromStrId(strId, tableNumId) {
        return __awaiter(this, void 0, void 0, function* () {
            var _a;
            let index = -1;
            const columnsInfo = this.internalColumnsRepr;
            for (const [i, colId] of columnsInfo.colId.entries()) {
                const belongsToTable = this.belongsToTable(tableNumId, columnsInfo.id[i]);
                if (colId == strId && belongsToTable) {
                    index = i;
                    break;
                }
            }
            if (index == -1) {
                return undefined;
            }
            return (_a = this.internalColumnsRepr.id) === null || _a === void 0 ? void 0 : _a[index];
        });
    }
    getIdFromLabel(label, tableNumId) {
        return __awaiter(this, void 0, void 0, function* () {
            var _a;
            let index = -1;
            const columnsInfo = this.internalColumnsRepr;
            for (const [i, colLabel] of columnsInfo.label.entries()) {
                const belongsToTable = this.belongsToTable(tableNumId, columnsInfo.id[i]);
                if (colLabel == label && belongsToTable) {
                    index = i;
                    break;
                }
            }
            if (index == -1) {
                return undefined;
            }
            return (_a = this.internalColumnsRepr.id) === null || _a === void 0 ? void 0 : _a[index];
        });
    }
    getProperty(property, id) {
        const idx = this.getIndex(id);
        if (idx) {
            return this.internalColumnsRepr[property][idx];
        }
        return null;
    }
    /** Lists the columns defining rules regarding the column with given string
     * Id
     *
     * @param {string} id
     *
     * @returns {?number[]}
     */
    getRules(id) {
        const childCols = this.getProperty("rules", id);
        if (!childCols) {
            return undefined;
        }
        // Discard first element, which is always a "L" letter
        const [, ...childColsCleaned] = childCols;
        return childColsCleaned;
    }
    getStrId(id) {
        return this.getProperty("colId", id);
    }
    getWidgetOptions(id) {
        const strOptions = this.getProperty("widgetOptions", id);
        if (strOptions) {
            return JSON.parse(strOptions);
        }
        return;
    }
    getDescription(id) {
        return this.getProperty("description", id);
    }
    getLabel(id) {
        return this.getProperty("label", id);
    }
    getTableLabels(tableNumId) {
        return __awaiter(this, void 0, void 0, function* () {
            const labels = [];
            for (const [i, colId] of this.internalColumnsRepr.id.entries()) {
                const colStrId = this.internalColumnsRepr.colId[i];
                if (this.belongsToTable(tableNumId, colId) &&
                    !colStrId.startsWith("gristHelper_")) {
                    labels.push(this.internalColumnsRepr.label[i]);
                }
            }
            return labels;
        });
    }
    static fetchData() {
        return __awaiter(this, void 0, void 0, function* () {
            const columnsInfo = yield grist.coreDocApi.fetchTable("_grist_Tables_column");
            printDebug(JSON.stringify(columnsInfo, null, 2));
            return columnsInfo;
        });
    }
    /** Retrieve the row index in the internal representation table
     */
    getIndex(id) {
        return this.indexById[id];
    }
    belongsToTable(tableNumId, colId) {
        const index = this.getIndex(colId);
        if (!index) {
            return false;
        }
        const tableNumIdForColumn = this.internalColumnsRepr.parentId[index];
        return tableNumIdForColumn == tableNumId;
    }
}
export function addFillColorToWidgetOptions(widgetOptions) {
    var _a;
    widgetOptions !== null && widgetOptions !== void 0 ? widgetOptions : (widgetOptions = {});
    (_a = widgetOptions.rulesOptions) !== null && _a !== void 0 ? _a : (widgetOptions.rulesOptions = []);
    widgetOptions.rulesOptions.push({ fillColor: "#FECBCC" });
    return widgetOptions;
}
export function deleteFillColorFromWidgetOptions(widgetOptions, indexToDelete) {
    var _a;
    (_a = widgetOptions === null || widgetOptions === void 0 ? void 0 : widgetOptions.rulesOptions) === null || _a === void 0 ? void 0 : _a.splice(indexToDelete, 1);
    return widgetOptions;
}
function printDebug(debug) {
    const containerId = "debug";
    const container = document.getElementById(containerId);
    if (container) {
        container.innerHTML = debug;
    }
    else {
        console.error(`Could not find container with id ${containerId} to print
    line errors`);
    }
}
export class TablesInfo {
    static init() {
        return __awaiter(this, void 0, void 0, function* () {
            return new TablesInfo(yield this.fetchData());
        });
    }
    constructor(internalTableRepr) {
        this.internalTableRepr = internalTableRepr;
        const numIdByStrId = {};
        internalTableRepr.tableId.forEach((strId, idx) => {
            numIdByStrId[strId] = internalTableRepr.id[idx];
        });
        this.numIdByStrId = numIdByStrId;
    }
    getNumId(strId) {
        return this.numIdByStrId[strId];
    }
    static fetchData() {
        return __awaiter(this, void 0, void 0, function* () {
            const columnsInfo = yield grist.coreDocApi.fetchTable("_grist_Tables");
            return columnsInfo;
        });
    }
}
