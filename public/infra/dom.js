var DisplayBy;
(function (DisplayBy) {
    DisplayBy[DisplayBy["Type"] = 0] = "Type";
    DisplayBy[DisplayBy["Field"] = 1] = "Field";
})(DisplayBy || (DisplayBy = {}));
export class DOMService {
    printErrors(errors, noErrorMsg, containerId, displayBy) {
        const container = document.getElementById(containerId);
        if (!container) {
            console.error(`Could not find container with id ${containerId} to print line errors`);
            return;
        }
        if (errors.length == 0) {
            container.innerHTML = noErrorMsg;
            return;
        }
        const htmlErrors = errors.reduce((htmlAccu, error) => {
            let label;
            if (displayBy == DisplayBy.Type) {
                label = error.code;
            }
            else {
                label = error.fieldName || "";
            }
            htmlAccu += "  <dt><mark>" + label + "</mark></dt>\n";
            htmlAccu += "  <dd>" + error.message + "</dd>\n";
            return htmlAccu;
        }, "");
        container.innerHTML = htmlErrors;
    }
    printStructureErrors(errors) {
        this.printErrors(errors, "Pas d'erreur de structure", "structure-errors", DisplayBy.Type);
    }
    printBodyErrors(errors) {
        this.printErrors(errors, "Pas d'erreur générale sur les données", "body-errors", DisplayBy.Type);
    }
    printRowErrors(errors) {
        this.printErrors(errors, "Pas d'erreur de validation pour la ligne sélectionnée", "row-errors", DisplayBy.Field);
    }
}
