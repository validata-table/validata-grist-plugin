var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { ColumnsInfo, TablesInfo, addFillColorToWidgetOptions, deleteFillColorFromWidgetOptions, } from "./grist-internals.js";
const VALIDATA_PLUGIN_WATERMARK = "_validata_plugin_watermark";
export class GristService {
    constructor() {
        /** Table ID is cached, and only updated when the data is fetched from the
         * table.
         *
         * Grist internals add hidden columns to define conditional formatting
         * formulas. It requires to pay attention on three spots of
         * `_grist_Tables_column` when adding,
         * updating or removing conditional formattings:
         *
         * - First, the formula is defined on the `formula` property of this hidden column,
         * - Then the link between the visible column and the related hidden column
         * is defined in the `rules` property of the visible column,
         * - At last, the conditional formatting color is defined in the
         * `widgetOptions.rulesOptions[i].fillColor` of the visible column, where position `i` relates to
         * position `i` in the rules
         **/
        this.cachedTableStrId = undefined;
        this.cachedTableNumId = undefined;
    }
    getColumnsInfo() {
        return __awaiter(this, void 0, void 0, function* () {
            return ColumnsInfo.init();
        });
    }
    invalidateTableIdCache() {
        this.cachedTableStrId = undefined;
        this.cachedTableNumId = undefined;
    }
    getTableStrId() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.cachedTableStrId) {
                this.cachedTableStrId = yield grist.getSelectedTableId();
            }
            return this.cachedTableStrId;
        });
    }
    getTableNumId() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.cachedTableNumId) {
                const tablesInfo = yield TablesInfo.init();
                const strId = yield this.getTableStrId();
                this.cachedTableNumId = tablesInfo.getNumId(strId);
            }
            return this.cachedTableNumId;
        });
    }
    fetchRecords() {
        return __awaiter(this, void 0, void 0, function* () {
            /** in case the selected table on the plugin has been changed **/
            this.invalidateTableIdCache();
            const columnsInfo = yield this.getColumnsInfo();
            const labels = yield columnsInfo.getTableLabels(yield this.getTableNumId());
            const records = yield grist.fetchSelectedTable({
                format: "columns",
            });
            if (!records) {
                console.error("Could not fetch data. This is an internal error");
                return;
            }
            return GristService.convertGristData(records, labels, yield this.getTableStrId());
        });
    }
    _fetchLabels() {
        return __awaiter(this, void 0, void 0, function* () {
            const columnsInfo = yield this.getColumnsInfo();
            return columnsInfo.getTableLabels(yield this.getTableNumId());
        });
    }
    hasConditionalFormatting(strId) {
        return __awaiter(this, void 0, void 0, function* () {
            const columnsInfo = yield this.getColumnsInfo();
            const id = yield columnsInfo.getIdFromStrId(strId, yield this.getTableNumId());
            if (!id) {
                return false;
            }
            const conditionalId = this._fetchConditionalFormattingId(id, columnsInfo);
            return conditionalId !== undefined;
        });
    }
    newConditionalFormatting(strId, formula) {
        return __awaiter(this, void 0, void 0, function* () {
            let columnsInfo = yield this.getColumnsInfo();
            const id = yield columnsInfo.getIdFromStrId(strId, yield this.getTableNumId());
            if (!id) {
                return;
            }
            // Action to add empty Conditional formatting rule
            const tableId = yield this.getTableStrId();
            const addEmptyRule = this._addEmptyRuleAction(tableId, id);
            const updateFillColor = this._addFillColorAction(id, columnsInfo);
            yield applyActions([addEmptyRule, updateFillColor]);
            // Adding conditional formatting adds a new row to internal columns information
            // Therefore, a data update is required.
            columnsInfo = yield columnsInfo.update();
            const rules = columnsInfo.getRules(id);
            if (!rules) {
                return;
            }
            const conditionalFormattingId = rules[rules.length - 1];
            if (!conditionalFormattingId) {
                return;
            }
            const updateFormula = this._updateFormulaAction(conditionalFormattingId, formula);
            yield applyActions([updateFormula]);
        });
    }
    updateConditionalFormatting(strId, formula) {
        return __awaiter(this, void 0, void 0, function* () {
            const columnsInfo = yield this.getColumnsInfo();
            const id = yield columnsInfo.getIdFromStrId(strId, yield this.getTableNumId());
            if (!id) {
                return;
            }
            const conditionalFormattingId = this._fetchConditionalFormattingId(id, columnsInfo);
            if (conditionalFormattingId) {
                const updateFormulaAction = this._updateFormulaAction(conditionalFormattingId, formula);
                yield applyActions([updateFormulaAction]);
            }
        });
    }
    deleteConditionalFormatting(strId) {
        return __awaiter(this, void 0, void 0, function* () {
            const columnsInfo = yield this.getColumnsInfo();
            const id = yield columnsInfo.getIdFromStrId(strId, yield this.getTableNumId());
            if (!id) {
                return;
            }
            const formattingId = this._fetchConditionalFormattingId(id, columnsInfo);
            if (!formattingId) {
                return;
            }
            const formattingStrId = columnsInfo.getStrId(formattingId);
            if (!formattingStrId) {
                return;
            }
            if (formattingId) {
                const deleteFormatting = yield this._deleteFormattingAction(formattingStrId);
                const deleteFillColor = this._deleteFillColorAction(id, formattingId, columnsInfo);
                if (deleteFillColor) {
                    yield applyActions([deleteFormatting, deleteFillColor]);
                }
            }
        });
    }
    _fetchConditionalFormattingId(id, columnsInfo) {
        const formattingIds = columnsInfo.getRules(id);
        if (formattingIds) {
            for (const id of formattingIds) {
                const ruleDescription = columnsInfo.getDescription(id);
                if (ruleDescription && ruleDescription == VALIDATA_PLUGIN_WATERMARK) {
                    return id;
                }
            }
        }
        return;
    }
    _addEmptyRuleAction(tableId, id) {
        return ["AddEmptyRule", tableId, 0, id];
    }
    _addFillColorAction(id, columnsInfo) {
        const widgetOptions = columnsInfo.getWidgetOptions(id);
        const newWidgetOptions = addFillColorToWidgetOptions(widgetOptions);
        return [
            "UpdateRecord",
            "_grist_Tables_column",
            id,
            { widgetOptions: JSON.stringify(newWidgetOptions) },
        ];
    }
    _deleteFillColorAction(id, formattingId, columnsInfo) {
        const rules = columnsInfo.getRules(id);
        if (!rules) {
            return rules;
        }
        const idx = rules.indexOf(formattingId);
        const widgetOptions = columnsInfo.getWidgetOptions(id);
        const newWidgetOptions = deleteFillColorFromWidgetOptions(widgetOptions, idx) || "";
        return [
            "UpdateRecord",
            "_grist_Tables_column",
            id,
            { widgetOptions: JSON.stringify(newWidgetOptions) },
        ];
    }
    _updateFormulaAction(conditionalFormattingId, formula) {
        return [
            "UpdateRecord",
            "_grist_Tables_column",
            conditionalFormattingId,
            { formula: formula, description: VALIDATA_PLUGIN_WATERMARK },
        ];
    }
    _deleteFormattingAction(conditionalFormattingStrId) {
        return __awaiter(this, void 0, void 0, function* () {
            const tableId = yield this.getTableStrId();
            return ["RemoveColumn", tableId, conditionalFormattingStrId];
        });
    }
    static convertGristData(gristCol, labels, tableId) {
        const idColumn = {
            id: "id",
            label: "",
            position: -1,
            values: gristCol.id,
        };
        const columns = [];
        Object.keys(gristCol).forEach((colId, idx) => {
            if (colId == "id") {
                return;
            }
            const newColumn = {
                id: colId,
                label: labels[idx],
                position: idx,
                values: gristCol[colId],
            };
            columns.push(newColumn);
        });
        return {
            idColumn: idColumn,
            columns: columns,
            nRows: gristCol.id.length,
            tableId: tableId,
        };
    }
}
function applyActions(actions) {
    return __awaiter(this, void 0, void 0, function* () {
        yield grist.coreDocApi.applyUserActions(actions);
    });
}
