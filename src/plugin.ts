import { highlightErrors } from "./formatting.js";

import type { IGrist, IValidata, IDOM } from "./spi.js";
import {
  type ValidationReport,
  type Error,
  relatesToDataStructure,
  relatesToDataBody,
  relatesToRow,
} from "./types/report.js";
import type { TableData } from "./types/records.js";

let validationReport: ValidationReport | undefined = undefined;

/** Validate data against the user provided schema.
 *
 * Cell and Line error messages are only displayed after the user selects the
 * a given line with errors.
 */
export async function validateTable(
  schemaURL: string,
  validataService: IValidata,
  gristService: IGrist,
  domService: IDOM,
) {
  const records = await gristService.fetchRecords();

  if (records) {
    const report = await validataService.requestValidataReport(
      records,
      schemaURL,
      {
        header_case: true,
      },
    );

    addRowIds(report, records);
    storeValidationReport(report);
    void highlightErrors(report, records, gristService);
    displayGeneralErrors(report, domService);
  }
}

/** Returns the last evaluated validation report, if any.
 */

export function getValidationReport(): ValidationReport | undefined {
  return validationReport;
}

/** Stores a validation report for future reference with `getValidationReport`
 */
function storeValidationReport(report: ValidationReport) {
  validationReport = report;
}

/** Given a report, displays all table errors to the user
 */
export function displayGeneralErrors(
  report: ValidationReport,
  domService: IDOM,
) {
  domService.printStructureErrors(extractStructureErrors(report));
  domService.printBodyErrors(extractBodyErrors(report));
}

/** Given a report, displays errors specific to line n to the user
 */
export function displayRowErrors(
  report: ValidationReport,
  rowId: number,
  domService: IDOM,
) {
  domService.printRowErrors(extractRowErrors(report, rowId));
}

function extractStructureErrors(report: ValidationReport): Error[] {
  const errors1 = report.report.errors || [];
  let errors2: Error[] = [];
  if (report.report.tasks.length > 0) {
    errors2 = report.report.tasks[0].errors;
  }

  const allErrors = errors1.concat(errors2);

  return allErrors.filter(relatesToDataStructure);
}

function extractBodyErrors(report: ValidationReport): Error[] {
  const errors1 = report.report.errors || [];
  let errors2: Error[] = [];
  if (report.report.tasks.length > 0) {
    errors2 = report.report.tasks[0].errors;
  }

  const allErrors = errors1.concat(errors2);

  return allErrors
    .filter(relatesToDataBody)
    .filter((err) => !relatesToRow(err));
}

function extractRowErrors(report: ValidationReport, rowId: number): Error[] {
  if (report.report.tasks.length == 0) {
    return [];
  }

  return report.report.tasks[0].errors
    .filter(relatesToRow)
    .filter((err) => err.rowId == rowId);
}

function addRowIds(report: ValidationReport, table: TableData) {
  for (const error of report.report.tasks[0].errors) {
    if (error.rowNumber) {
      const rowId = table.idColumn.values[error.rowNumber - 1];
      error.rowId = rowId;
    }
  }
}
