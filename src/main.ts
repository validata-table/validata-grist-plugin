import { validateTable, getValidationReport } from "./plugin.js";
import { displayRowErrors } from "./plugin.js";
import { GristService } from "./infra/grist.js";
import { DOMService } from "./infra/dom.js";
import { ValidataService } from "./infra/validata.js";

const gristService = new GristService();
const domService = new DOMService();
const validataService = new ValidataService();

grist.ready({
  requiredAccess: "full",
});

grist.onRecord(async (row) => {
  const report = getValidationReport();
  if (report && row) {
    displayRowErrors(report, row.id, domService);
  }
});

const SCHEMA_FORM_ID = "schema-form";
const schemaForm = document.getElementById(SCHEMA_FORM_ID);

// Any form submit (re)triggers a validation
if (schemaForm) {
  schemaForm.addEventListener("submit", validate);
}

async function validate(event: SubmitEvent) {
  event.preventDefault(); // do not refresh page
  const schemaURL = _get_schema_url(event);

  if (schemaURL) {
    void validateTable(schemaURL, validataService, gristService, domService);
  }
}

/**
 * @returns <?string> URL to a Table schema descriptor
 */
function _get_schema_url(event: SubmitEvent) {
  const target = event.target;
  if (!target || !(target instanceof HTMLFormElement)) {
    return;
  }

  const data = new FormData(target);
  const schema = data.get("schema");
  if (!schema || !(typeof schema === "string")) {
    console.error(
      "The schema input is not defined, not provided as a string, or empty (i.e. not set as required). This is an internal error.",
    );
    return;
  }

  return schema;
}
