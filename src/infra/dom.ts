import { IDOM } from "../spi.js";

import { Error } from "../types/report.js";

enum DisplayBy {
  Type,
  Field,
}

export class DOMService implements IDOM {
  private printErrors(
    errors: Error[],
    noErrorMsg: string,
    containerId: string,
    displayBy: DisplayBy,
  ) {
    const container = document.getElementById(containerId);
    if (!container) {
      console.error(
        `Could not find container with id ${containerId} to print line errors`,
      );
      return;
    }

    if (errors.length == 0) {
      container.innerHTML = noErrorMsg;
      return;
    }

    const htmlErrors = errors.reduce((htmlAccu, error) => {
      let label: string;
      if (displayBy == DisplayBy.Type) {
        label = error.code;
      } else {
        label = error.fieldName || "";
      }

      htmlAccu += "  <dt><mark>" + label + "</mark></dt>\n";

      htmlAccu += "  <dd>" + error.message + "</dd>\n";

      return htmlAccu;
    }, "");
    container.innerHTML = htmlErrors;
  }

  printStructureErrors(errors: Error[]) {
    this.printErrors(
      errors,
      "Pas d'erreur de structure",
      "structure-errors",
      DisplayBy.Type,
    );
  }

  printBodyErrors(errors: Error[]) {
    this.printErrors(
      errors,
      "Pas d'erreur générale sur les données",
      "body-errors",
      DisplayBy.Type,
    );
  }

  printRowErrors(errors: Error[]) {
    this.printErrors(
      errors,
      "Pas d'erreur de validation pour la ligne sélectionnée",
      "row-errors",
      DisplayBy.Field,
    );
  }
}
